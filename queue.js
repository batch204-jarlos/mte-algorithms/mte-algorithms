let collection = [];

// Write the queue functions below.

// Usage of JS Array Methods (push, pop, shift, unshift, etc) is not allowed

// Print output of all the elements in queue
function print() {
    return collection;
} 

// Add element to rear of queue
function enqueue(element) {
   collection[collection.length] = element
   return collection;

}

// Remove element at front of queue
function dequeue() {

    for (let i = 1; i < collection.length; i++)
        collection[i - 1] = collection[i];
        collection.length--;
    return collection;
}

// shows element at the front
function front() {
    return collection[0];
}

// Show the total number of elements
function size() {
    return collection.length;
}

// outputs boolean value describing whether queue is empty or not
function isEmpty() {
    if (collection.length === 0){
        return  true
    } else {
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};